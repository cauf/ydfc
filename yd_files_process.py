from typing import Any, Dict, List, Tuple, Union
import requests as rq
from pathlib import Path


BASE_URL = 'https://cloud-api.yandex.net/v1/disk/public/resources'
INDENT_LENGTH = 4
INDENT_STR = " "*INDENT_LENGTH


class ResponceError(Exception):
    pass


class SizeUnit:
    def __init__(self, size_in_bytes:int) -> None:
        self.b:int = size_in_bytes
        self.kb:float = self.b / 1024
        self.mb:float = self.kb / 1024
        self.gb:float = self.mb / 1024
        self.tb:float = self.gb / 1024
    
    @property
    def value(self) -> str:
        num_value:float = 0
        unit:str = ''
        if self.b < 1024:
            num_value = self.b
            unit = 'б'
        elif self.kb < 1024:
            num_value = self.kb
            unit = 'Кб'
        elif self.mb < 1024:
            num_value = self.mb
            unit = 'Мб'
        elif self.gb < 1024:
            num_value = self.gb
            unit = 'Гб'
        elif self.tb < 1024:
            num_value = self.tb
            unit = 'Тб'
        return f'{num_value:.2f} {unit}'


class ContentFile:
    def __init__(self, data:Dict[str, Any]=None) -> None:
        self.path:str = data['path'] if data else None
        self.name:str = data['name'] if data else None
        self.md5:str = data['md5'] if data else None
        self.size:int = data['size'] if data else None
        self.sha256:str = data['sha256'] if data else None
        self.type:str = data['media_type'] if data else None
        self.in_remote:bool = data is not None
        self.in_local:bool = False
        self.is_different:bool = True
        self.local_path:Path = None

    def get_diff(self) -> str:
        return f'{self.type if self.type else "unexplained"} {self.name}'

    def __str__(self) -> str:
        return f'{self.type if self.type else "unexplained"} {self.path}'
    
    def __repr__(self) -> str:
        return f'<ContentFile {self.path!r}>'


class FileDiff:
    def __init__(
        self, 
        name:str, 
        path:str, 
        in_remote:bool=False, 
        in_local:bool=False,
    ) -> None:
        self.name = name
        self.path = path
        self.in_remote = in_remote
        self.in_local = in_local


class DirDiff(FileDiff):
    pass

class ContentDir:
    def __init__(self, data:Dict[str, Any]=None, indent:int=0) -> None:
        self.items:List[Union[ContentFile, ContentDir]] = []
        self.path:str = data['path'] if data else None
        self.name:str = data['name'] if data else None
        self.public_key:str = data['public_key'] if data else None
        self.indent:int = indent
        self.in_remote:bool = data is not None
        self.in_local:bool = False
        self.local_path:Path = None
        self.size:int = 0
        if data:
            self._get_dir_content()
    
    @staticmethod
    def load_from_public_key(public_key:str, path:str='/', name:str='') -> 'ContentDir':
        res = ContentDir()
        res.public_key = public_key
        res.path = path
        res.name = name
        res._get_dir_content()
        return res

    @property
    def files(self) -> List[ContentFile]:
        return [item for item in self.items if isinstance(item, ContentFile)]
    
    @property
    def files_names(self) -> List[str]:
        return [item.name for item in self.files]

    @property
    def dirs(self) -> List['ContentDir']:
        return [item for item in self.items if isinstance(item, ContentDir)]
    
    @property
    def dirs_names(self) -> List[str]:
        return [item.name for item in self.dirs]

    @property
    def is_different(self) -> bool:
        for item in self.items:
            if item.is_different:
                return True
        return False

    def _get_dir_content(self) -> List[Union['ContentDir', 'ContentFile']]:
        resp = rq.get(
            BASE_URL, 
            params={
                'public_key': self.public_key,
                'path': self.path if self.path else '/',
                'limit': 1000,
            },
            timeout=60,
        )
        
        if not resp.ok:
            raise ResponceError(f"Получен некорретный ответ от удаленного сервера:\n{resp.raw}")
        
        self.in_remote = True
        data = resp.json()
        for item in data['_embedded']['items']:
            if item['type'] == 'file':
                obj_item = ContentFile(item)
            elif item['type'] == 'dir':
                obj_item = ContentDir(item, self.indent+1)
            self.size += obj_item.size
            self.items.append(obj_item)

    def compare_with_local_path(self, local_path:Path) -> None:
        for item in local_path.iterdir():
            if item.is_file():
                if item.name in self.files_names:
                    content = [fl for fl in self.files if fl.name == item.name][0]
                    content.is_different = False
                else:
                    content = ContentFile()
                    content.name = item.name
                    self.items.append(content)
            else:
                if item.name in self.dirs_names:
                    content = [dr for dr in self.dirs if dr.name == item.name][0]
                else:
                    content = ContentDir(indent=self.indent+1)
                    content.name = item.name
                    self.items.append(content)
                content.compare_with_local_path(item)
            content.in_local = True
            content.local_path = item

    def get_diff(self) -> Dict[Union[str, DirDiff], Any]:
        result = {
            '':[],
        }
        for item in self.items:
            if item.is_different:
                if isinstance(item, ContentFile):
                    result[''].append(
                        FileDiff(
                            item.name,
                            item.path,
                            item.in_remote,
                            item.in_local,
                        )
                    )
                elif isinstance(item, ContentDir):
                    dir_diff = DirDiff(
                        item.name,
                        item.path,
                        item.in_remote,
                        item.in_local,
                    )
                    content = item.get_diff()
                    result[dir_diff] = content
        return result

    def get_subdirs_with_size(self) -> Dict[str, Tuple[SizeUnit, str, Dict]]:
        return {
            dir_.name: (
                SizeUnit(dir_.size),
                dir_.path,
                dir_.get_subdirs_with_size()
            )
            for dir_ in self.dirs
        }

    def __str__(self) -> str:
        result = []
        for item in self.items:
            cur_indent = self.indent if isinstance(self, ContentDir) else self.indent+1
            ln = f'{cur_indent*INDENT_STR}{item}'
            result.append(ln)
        content = '\n'.join(result)
        return f'dir {self.path}\n{content}'
    
    def __repr__(self) -> str:
        return f'<ContentDir {self.path!r}>'


if __name__ == '__main__':
    su = SizeUnit(4961234)
    print(su.value)
