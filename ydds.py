from argparse import ArgumentParser
from datetime import datetime as dt
from typing import Dict, Tuple
from yd_files_process import ContentDir, SizeUnit, INDENT_STR


def render_subdirs(
    subdirs_data: Dict[str, Tuple[SizeUnit, str, Dict]], 
    indent:int=0,
) -> str:
    results = []
    for dir_name, dir_content in subdirs_data.items():
        size, path, subdirs = dir_content
        ln = f'{indent*INDENT_STR}{dir_name} : {size.value}'
        if subdirs:
            ln = f'{ln}\n{render_subdirs(subdirs, indent+1)}'
        results.append(ln)
    return '\n'.join(results)


def get_subdirs(url:str) -> str:
    """
    Формирует текст результатов анализа размера каталогов

    :param url: URL-адрес (либо публичный ключ) на опубликованный каталог
    :type url: str
    :return: текст результатов анализа размера каталогов
    :rtype: str
    """    
    content = ContentDir.load_from_public_key(public_key=args.url)
    rendered_data = render_subdirs(
        content.get_subdirs_with_size(),
        0,
    )
    return rendered_data


if __name__ == '__main__':
    start = dt.now()
    parser = ArgumentParser()
    parser.add_argument('url', help='url-address to yandex-disk folder',type=str)
    args = parser.parse_args()
    result = get_subdirs(args.url)
    print('='*80)
    print(result)
    print('='*80)
    finish = dt.now()
    delta = finish - start
    print('Выполнено за:', delta.seconds, ' сек')
