from tkinter import ttk
import tkinter as tki
from tkinter.constants import NO
from typing import Any, Dict, Tuple, Union
from yd_files_process import ContentDir, DirDiff, FileDiff, SizeUnit
from pathlib import Path


class AboutWindow(tki.Toplevel):
    def __init__(self, master) -> None:
        super().__init__(master)


class MainWindow():
    def __init__(self) -> None:
        self.content: ContentDir = None

        self._root = tki.Tk()
        self._root.wm_title("Оценка размера каталогов Yandex.Disk")
        self._root.wm_minsize(width=640,height=480)
        self._root.wm_resizable(False, False)

        # URL
        self.url_label = tki.Label(self._root, text="URL")
        self.url_label.grid(column=0, row=0)

        self.url_entry = ttk.Entry(self._root,width=100)
        self.url_entry.grid(column=1, row=0, sticky="EW", columnspan=2)

        # ButtonFrame
        self.button_frame = tki.Frame(self._root, width=640)
        self.calc_size_button = ttk.Button(self.button_frame, text='Оценить размер', command=self.update_results)
        self.button_frame.grid(column=0, row=1, columnspan=3, sticky='WE')
        self.calc_size_button.grid(column=0, row=0, sticky='WE', pady=10)

        # Results
        self.result_label = tki.Label(self._root, text='Результат оценки')
        self.result_label.grid(column=0, row=2, columnspan=3, sticky='W')

        self.result_treeview = ttk.Treeview(
            self._root, 
            show='tree headings', 
            columns=('#1', '#2'),
            height=17
        )
        self.result_treeview.heading('#0', text='Название')
        self.result_treeview.heading('#1', text='Путь')
        self.result_treeview.heading('#2', text='Размер')
        self.result_treeview.column('#1', width=200)
        self.result_treeview.column('#2', width=20)
        self.result_treeview.grid(
            column=0, 
            row=3, 
            columnspan=3, 
            sticky='NESW', 
            ipady=5,
        )

        # Statusbar
        self.statusbar = tki.Label(
            self._root, 
            text='Для оценки размера каталогов введите URL и нажмите кнопку', 
            justify='left',
        )
        self.statusbar.grid(column=0, row=4, columnspan=3, sticky='W', ipady=5)

    def run(self) -> None:
        self._root.mainloop()

    def set_status(self, status:str) -> None:
        self.statusbar.configure(text=status)

    def _update_input(self) -> None:
        self.url:str = self.url_entry.get()
        if not self.url:
            self.set_status("ОШИБКА!!! Не указан URL-адрес")
            return -1

    def _set_result_item(
        self, 
        parent:str, 
        name:str, 
        path:str, 
        size:str,
    ) -> str:

        return self.result_treeview.insert(
            parent, 
            index='end', 
            text=name, 
            values=(path, size),
        )

    def update_results(self) -> None:
        self._request()
        if not self.sizes:
            self.set_status('ОШИБКА!!! Нет сформированных результатов')
            return
        self.insert_results(self.sizes)
        self.set_status("Готово!")

    def _request(self) -> None:
        self.set_status('Получение данных из удаленного источника')
        if self._update_input() == -1:
            return
        self.content = ContentDir.load_from_public_key(public_key=self.url)
        self.sizes = self.content.get_subdirs_with_size()
    
    def insert_results(
        self, 
        data:Dict[str, Tuple[SizeUnit, str, Dict]], 
        parent:str='',
    ) -> None:
        self.set_status('Вывод результатов')
        for dir_name, dir_content in data.items():
            size, path, subdirs = dir_content
            dir_index = self._set_result_item(parent, dir_name, path, size.value)
            if subdirs:
                self.insert_results(subdirs, dir_index)

if __name__ == '__main__':
    wnd = MainWindow()
    wnd.run()
