from argparse import ArgumentParser
from datetime import datetime as dt
from pathlib import Path
from typing import Any, Dict, List, Union
from yd_files_process import (
    INDENT_STR,
    ContentDir,
    DirDiff,
)


def render_diff(diff:Dict[Union[str, DirDiff], Any], indent:int=0) -> str:
    """
    Функция для преобразования результата сравнения в текстовую форму

    :param diff: результат сравнения удаленного и локального каталогов
    :type diff: Dict[Union[str, DirDiff], Any]
    :param indent: начальное значение отступа, по-умолчанию = 0
    :type indent: int, optional
    :return: текстовое представление сравнения удаленного и локального каталогов
    :rtype: str
    """    
    dir_: Union[str,DirDiff]
    content: Any
    result = []
    for dir_, content in diff.items():
        if dir_ == '':
            for fl in content:
                ln = f"{INDENT_STR*indent}{fl.name}"
                result.append(ln)
        elif isinstance(dir_, DirDiff):
            dir_content = render_diff(content, indent+1)
            ln = f"{INDENT_STR*indent}{dir_.name}\n{dir_content}"
            result.append(ln)
    return '\n'.join(result)


def decorate_diff(rendered_diff:str, url:str, path:Path) -> str:
    """
    Добавляет к текстовому представлению результатов сравнения удаленного и 
    локального каталогов верхний калонтитул, представляющий собой значения 
    введенных аргументов и заголовки с разделам выводимой информации

    :param rendered_diff: текстовое представление результатов сравнения 
        удаленного и локального каталогов. Результат выполнения функции
        `render_diff`
    :type rendered_diff: str
    :param url: URL-адрес (либо публичный ключ) на опубликованный каталог
    :type url: str
    :param path: путь к локальному каталогу
    :type path: Path
    :return: Отдекорированный текст результатов сравнения удаленного и 
        локального каталогов
    :rtype: str
    """    
    content = rendered_diff if rendered_diff else f'{INDENT_STR}Not found'
    now = dt.now()
    return (
        f'Current date and time: {now.strftime("%d.%m.%Y %H:%M:%S")}\n\n'
        'Entered arguments\n'
        f'{INDENT_STR}URL: {url}\n'
        f'{INDENT_STR}Path: {path}\n\n'
        "Files that don't match:\n"
        f'{content}'
    )


def get_diff(url:str, path:Path) -> str:
    """
    Формирует текст результатов сравнения удаленного и локального каталогов

    :param url: URL-адрес (либо публичный ключ) на опубликованный каталог
    :type url: str
    :param path: путь к локальному каталогу
    :type path: Path
    :return: текст результатов сравнения удаленного и локального каталогов
    :rtype: str
    """    
    content = ContentDir.load_from_public_key(public_key=args.url)
    content.compare_with_local_path(path)
    diff = content.get_diff()
    rendered_diff = render_diff(diff, 1)
    return decorate_diff(rendered_diff, url, path)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('url', help='url-address to yandex-disk folder',type=str)
    parser.add_argument('path', help='path to local folder', type=str)
    args = parser.parse_args()
    local_path = Path(args.path).resolve()
    diff = get_diff(args.url, local_path)
    print(diff)
