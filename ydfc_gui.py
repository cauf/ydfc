from tkinter import ttk
import tkinter as tki
from tkinter.constants import NO
import tkinter.filedialog as tfd
from typing import Any, Dict, Union
from yd_files_process import ContentDir, DirDiff, FileDiff
from pathlib import Path


class AboutWindow(tki.Toplevel):
    def __init__(self, master) -> None:
        super().__init__(master)


class MainWindow():
    def __init__(self) -> None:
        self.content: ContentDir = None
        self.diff: Dict[str, Union[DirDiff, Any]] = None

        self._root = tki.Tk()
        self._root.wm_title("Сравнение каталогов Yandex.Disk")
        self._root.wm_minsize(width=640,height=480)
        self._root.wm_resizable(False, False)

        # URL
        self.url_label = tki.Label(self._root, text="URL")
        self.url_label.grid(column=0, row=0)

        self.url_entry = ttk.Entry(self._root,width=100)
        self.url_entry.grid(column=1, row=0, sticky="EW", columnspan=2)

        # Path
        self.path_label = tki.Label(self._root, text="Путь")
        self.path_label.grid(column=0, row=1)

        self.path_entry = ttk.Entry(self._root,width=100)
        self.path_entry.grid(column=1, row=1, sticky="EW")

        self.path_browse_button = ttk.Button(
            self._root, 
            text="...", 
            width=2, 
            command=self._handler_browse_path,
        )
        self.path_browse_button.grid(column=2, row=1, sticky="EW")

        # ButtonFrame
        self.button_frame = tki.Frame(self._root, width=640)
        self.compare_button = ttk.Button(self.button_frame, text='Сравнить', command=self.update_results)
        self.button_frame.grid(column=0, row=2, columnspan=3, sticky='WE')
        self.compare_button.grid(column=0, row=0, sticky='WE', pady=10)

        # Results
        self.result_label = tki.Label(self._root, text='Результат сравнения')
        self.result_label.grid(column=0, row=3, columnspan=3, sticky='W')

        self.result_treeview = ttk.Treeview(
            self._root, 
            show='tree headings', 
            columns=('#1', '#2', '#3'),
            height=17
        )
        self.result_treeview.heading('#0', text='Название')
        self.result_treeview.heading('#1', text='Путь')
        self.result_treeview.heading('#2', text='Удаленно')
        self.result_treeview.heading('#3', text='Локально')
        self.result_treeview.column('#1', width=200)
        self.result_treeview.column('#2', width=20)
        self.result_treeview.column('#3', width=20)
        self.result_treeview.grid(column=0, row=4, columnspan=3, sticky='NESW', ipady=5)

        # Statusbar
        self.statusbar = tki.Label(self._root, text='Введите данные в поля URL и Путь', justify='left')
        self.statusbar.grid(column=0, row=5, columnspan=3, sticky='W', ipady=5)

    def run(self) -> None:
        self._root.mainloop()

    def set_status(self, status:str) -> None:
        self.statusbar.configure(text=status)

    def _update_input(self) -> None:
        self.url:str = self.url_entry.get()
        path_str: str = self.path_entry.get()
        self.path:Path = Path(path_str) if path_str else None
        if not self.url:
            self.set_status("ОШИБКА!!! Не указан URL-адрес")
            return -1
        if (not self.path) or (not self.path.exists()):
            self.set_status("ОШИБКА!!! Указан некорректный Путь")
            return -1

    def _set_result_item(self, parent:str, name:str, path:str, in_remote:bool, in_local:bool) -> str:

        return self.result_treeview.insert(
            parent, 
            index='end', 
            text=name, 
            values=(
                path, 
                '+' if in_remote else '-',
                '+' if in_local else '-',
            ),
        )

    def _process_diff_item(self, diff:Dict[Union[str, DirDiff], Any], parent_index:str='') -> None:
        for dir_, content in diff.items():
            if dir_ == '':
                for file_diff in content:
                    self._set_result_item(parent_index, file_diff.name, file_diff.path, file_diff.in_remote, file_diff.in_local)
            elif isinstance(dir_, DirDiff):
                dir_index = self._set_result_item(parent_index, dir_.name, dir_.path, dir_.in_remote, dir_.in_local)
                self._process_diff_item(content, parent_index=dir_index)

    def update_results(self) -> None:
        self._request()
        if not self.diff:
            self.set_status('ОШИБКА!!! Нет сформированных результатов сравнения')
            return
        self._process_diff_item(self.diff)
        self.set_status("Готово!")

    def _request(self) -> None:
        if self._update_input() == -1:
            return
        self.content = ContentDir.load_from_public_key(public_key=self.url)
        self.content.compare_with_local_path(self.path)
        self.diff = self.content.get_diff()

    def _handler_browse_path(self):
        path:str = tfd.askdirectory()
        self.path_entry.delete(0, tki.END)
        self.path_entry.insert(0, path)


if __name__ == '__main__':
    wnd = MainWindow()
    wnd.run()
